from collections import defaultdict

duplicate_key_set = set("x%d"%i for i in xrange(1, 21) if i%2 == 0)

items = 20

class Value:
    def __init__(self, time, val):
        self.time = time
        self.val = val

class Server:
    def __init__(self, id, time):
        self.id = id
        self.begin = time
        self.prepared = set()
        self.seen = set()
        self.hostRead = set()
        self.db = defaultdict(list)
        self.status = False
        self.__initialize__()

    def __initialize__(self):
        # set up initial db values
        for key in duplicate_key_set:
            val = Value(self.begin, int(key[1:])*10)
            self.db[key] = [val]

        # non duplicated val
        if self.id % 2 == 0:
            o1 = self.id - 1
            o2 = self.id + 9
            v1 = Value(self.begin, o1*10)
            v2 = Value(self.begin, o2*10)
            self.db["x%d"%o1] = [v1]
            self.db["x%d"%o2] = [v2]

        self.hostRead = set(self.db.keys())
        self.status = True

    def restart(self):
        self.seen = set()
        self.prepared = set()
        self.hostRead = set() if self.id % 2 != 0 else set(["x%i"%(self.id -1), "x%i"%(self.id + 9)])
        self.status = True

    def commit(self, tx, time):
        if not self.status or tx not in self.seen:
            return 0
        
        self.seen.remove(tx)

        if tx in self.prepared:
            
            for k in tx.writes:
                if k in self.hostRead or k in duplicate_key_set:
                    self.hostRead.add(k)
                    for val in tx.writes[k]:
                        self.db[k].append(Value(time, int(val)))
        
            self.prepared.remove(tx)

        return 1

    def abort(self, tx):
        if not self.status or tx not in self.seen:
            return None

        if tx in self.prepared:
            self.prepared.remove(tx)

        self.seen.remove(tx)
        return 1

    def get(self, tx, key):
        if not self.status or key not in self.hostRead:
            return None

        # no RO read and leaves record to the server
        if not tx.RO:
            self.seen.add(tx)
            return self.db[key][-1].val
        
        # RO read latest before begin + leaves no record
        for i in xrange(len(self.db[key])-1, -1, -1):
            if self.db[key][i].time < tx.begin:
                return self.db[key][i].val

        return None

    def put(self, tx, key, val):
        if not self.status or (key not in duplicate_key_set and key not in self.hostRead):
            return 0
        
        self.seen.add(tx)
        self.prepared.add(tx)
        return 1

    # return tx that should be abort
    def fail(self):
        self.status = False
        return self.seen

    def dump(self):
        msg = "===================\n" + "Site %d active: %r\n"%(self.id, self.status)
        for k in sorted(self.db):
            msg += "(%s, %d)\n"%(k, self.db[k][-1].val)
        msg += "END OF DB\n"
        return msg

    def dumpKey(self, key):
        msg = "===================\n" + "Site %d active: %r\n"%(self.id, self.status)
        if key in self.hostRead:
            msg += "(%s, %d)"%(key, self.db[key][-1].val)
        else:
            msg +=  "NOT AVAILABLE " + key
        return msg + "\n"