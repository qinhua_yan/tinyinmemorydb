from collections import defaultdict
from random import random, shuffle
import sys

from Transaction import *
from Server import *
from Job import *


sites = 10

START, READY, READ, COMMIT, ABORT, UNKNOWN = "Start", "Write Ready", "Read Got", "Commit", "Abort", "Not Exist"

ABORT_LOCK, ABORT_SERVER = "Older tx hold lock.",  "Server failure."
ABORT_NOT_AVAILABLE = "No servers available."

WAIT_LOCK, WAIT_SERVER = "Wait For Lock", "Wait, No Servers Available."

DOWN, RESTART = "Shuts Down", "Restart"


def BuildMessage(*args):
    msg = args[0].strip() + "::"
    for a in args[1:]: msg += str(a) + " "
    return msg + "\n"


class TransactionManager:
    def __init__(self):
        self.time = 0
        self.servers = {} # id to server instance
        self.aliveTx = {} # id to tx
        self.lock = defaultdict(list) # key x, value queue of Job
        
        # initialize WLock, servers
        for i in xrange(1, 1+sites):
            self.servers[i] = Server(i, self.time)

        for i in xrange(1, 1+items):
            self.lock["x%d"%i] = []

    def __dump__(self):
        msg = ""
        for server in self.servers.values():
            msg += server.dump()
        return msg

    def process(self):
        # process stdin where each line is a one or more instructions
        for line in sys.stdin:
            instructions = line.split(";")
            self.time += 1

            # shulffle instructions (fairness) not in order of appearance
            shuffle(instructions)
            for instruction in instructions:
                self.__process__(instruction.strip())

    def __process__(self, ins):
        # process an ins
        msg = BuildMessage(ins, "=====>")
        a = b = -1
        
        try: a, b = ins.index('('), ins.index(')')
        except ValueError: return
       
        args = ins[a+1:b]
        
        if ins.startswith("begin"):
            msg += self.__beginHandler__(ins[:a], args)
        
        elif ins.startswith("W("):
            msg += self.__writeHandler__(args.split(","))
        
        elif ins.startswith("R("):
            msg += self.__readHandler__(args.split(","))
        
        elif ins.startswith("end"):
            msg += self.__endHandler__(args)
        
        elif ins.startswith("fail"):
            msg += self.__failHandler__(args)

        elif ins.startswith("recover"):
            msg += self.__recoverHandler__(args)

        elif ins.startswith("dump"):
            args.strip()
            if args == "":
                msg += self.__dump__()
            elif args.startswith("x"):
                for site in self.servers:
                    msg += self.servers[site].dumpKey(args)
            else:
                msg += self.servers[int(args)].dump()

        print msg

    def __beginHandler__(self, header, tx_id):
        tx_id = tx_id.strip()
        newTx = Transaction(self.time, tx_id, header.endswith("RO"))
        self.aliveTx[tx_id] = newTx
        return BuildMessage(tx_id, START)

    def __writeHandler__(self, args):
        args = [a.strip() for a in args]
        tx_id, x, val = args

        if tx_id not in self.aliveTx:
            return BuildMessage(tx_id, UNKNOWN)

        tx = self.aliveTx[tx_id]

        # first time see, need lock
        if x not in tx.writes:
            # find position of previous write, if none, start from 0
            i = max(self.__prevWrite__(x), 0)
            while i < len(self.lock[x]):
                pjob = self.lock[x][i]
                # lock hold by older tx, abort
                if pjob.transaction.begin < tx.begin and (pjob.isWrite or pjob.transaction.reads[x]):
                    amsg, notifications = self.__abort__(tx)
                    return BuildMessage(amsg, ABORT_LOCK) + notifications
                i += 1
            # none of them is older, append
            newJob = Job(tx, val, True)
            self.lock[x].append(newJob)
        
        tx.writes[x].append(val)
        return self.__serversWrite__(tx, x, val)

    def __readHandler__(self, args):
        args = [a.strip() for a in args]
        tx_id, x = args
        
        if tx_id not in self.aliveTx:
            return BuildMessage(tx_id, UNKNOWN)

        tx = self.aliveTx[tx_id]

        # tried write it before
        if x in tx.writes:
            tx.reads[x] = tx.writes[x][-1]
            return BuildMessage(tx_id, READ, x, tx.writes[x][-1])

        # tried read it before
        if x in tx.reads:
            if tx.reads[x]:
                return BuildMessage(tx_id, READ, x, tx.reads[x])

            # still waiting
            if not tx.RO:
                return BuildMessage(tx_id, WAIT_LOCK, x)

        # This is the first time tx access x
        tx.reads[x] = None

        # not read only, need lock
        if not tx.RO:
            tx.reads[x] = None

            # find last write job in the lock queue
            i = self.__prevWrite__(x)

            # found last write, an older one, abort
            if i >= 0 and self.lock[x][i].transaction.begin < tx.begin:
                amsg, notifications = self.__abort__(tx)
                return BuildMessage(amsg, ABORT_LOCK) + notifications
                
            # make new job
            newJob = Job(tx)
            self.lock[x].append(newJob)

            # last write is younger, wait
            if i >= 0: 
                return BuildMessage(tx_id, WAIT_LOCK, x)
            
        # ReadOnly Tx/Got Lock No Wait -- try read now
        return self.__serversRead__(tx, x)

    def __endHandler__(self, tx_id):
        tx_id = tx_id.strip()

        if tx_id not in self.aliveTx:
            return BuildMessage(tx_id, UNKNOWN)

        tx = self.aliveTx[tx_id]

        # READ ONLY, commit directly
        if tx.RO:
            del self.aliveTx[tx_id]
            return BuildMessage(tx_id, COMMIT)

        # ask servers to commit
        count = 0
        for site in self.servers:
            count += self.servers[site].commit(tx, self.time)

        if count == 0:
            amsg, notifications = self.__abort__(tx)
            return BuildMessage(amsg, ABORT_NOT_AVAILABLE) + notifications

        msg = self.__releaseLock__(tx)

        return BuildMessage(tx.id, COMMIT) + msg

    def __failHandler__(self, site):
        site_id = "Site"+site
        site = int(site)

        toBAbort = self.servers[site].fail()

        msg = BuildMessage(site_id, DOWN)
        for tx in toBAbort:
            amsg, notifications = self.__abort__(tx)
            msg += BuildMessage(amsg, ABORT_SERVER)
            msg += notifications
        
        return msg

    def __recoverHandler__(self, site):
        site_id = "Site"+site
        site = int(site)

        self.servers[site].restart()
        msg = BuildMessage(site_id, RESTART)

        # SIGNAL waiting writes&reads for non-duplicated object
        if site % 2 == 0:
            ks = ["x%d"%(site-1), "x%d"%(site+9)]
            # waiting read-only are in self.aliveTx
            for tx_id in self.aliveTx:
                tx = self.aliveTx[tx_id]
                if not tx.RO:
                    continue

                for k in tx.reads:
                    # wait for reads
                    if k in ks and not tx.reads[k]:
                        val = self.servers[site].get(tx, k)
                        if val:
                            tx.reads[k] = val
                            msg += BuildMessage(tx.id, READ, k, val)
            for k in ks:
                # waiting reads are in front of the queue [0..index_of_first_write]
                for i in xrange(len(self.lock[k])):
                    job = self.lock[k][i]
                    if job.isWrite:
                        break
                    tx = job.transaction
                    if not self.aliveTx[tx.id].reads[k]:
                        val = self.servers[site].get(tx, k)
                        if val:
                            tx.reads[k] = val
                            msg += BuildMessage(tx.id, READ, k, val)

                # waiting writes are in job queue
                for job in self.lock[k]:
                    if job.isWrite:
                        rst = self.servers[site].put(job.transaction, k, job.value)
                        if rst:
                            msg += BuildMessage(job.transaction.id, READY, k, job.value)

        return msg

    def __queueIndex__(self, key, tx, val, iswrite=False, start=0):
        i = start
        while i < len(self.lock[key]):
            item = self.lock[key][i]
            if item.transaction == tx and item.isWrite == iswrite:
                if not val or item.value == val:
                    return i
            i += 1
        return -1

    def __prevWrite__(self, key, start=None):
        if not start:
            start = len(self.lock[key]) - 1
        i = start
        while i >= 0:
            if self.lock[key][i].isWrite:
                return i
            i -= 1
        return -1


    def __abort__(self, tx):
        for site in self.servers:
            self.servers[site].abort(tx)

        notifications = self.__releaseLock__(tx)
        msg = BuildMessage(tx.id, ABORT)

        return msg, notifications

    def __serversRead__(self, tx, key):
        site = 1 + int(random()*sites)
        for i in xrange(sites):
            site %= sites
            site += 1
            rst = self.servers[site].get(tx, key)
            if rst:
                tx.reads[key] = rst
                return BuildMessage(tx.id, READ, key, rst)

        # no sites success, wait
        return BuildMessage(tx.id, WAIT_SERVER, key)

    def __serversWrite__(self, tx, key, val):
        count = 0
        for site in self.servers:
            count += self.servers[site].put(tx, key, val)

        if count == 0:
            # no sites success, wait
            return BuildMessage(tx.id, WAIT_SERVER, key)

        return BuildMessage(tx.id, READY, key, val)

    def __releaseLock__(self, tx):
        msg = ""

        for k in tx.writes:
            p = self.__queueIndex__(k, tx, None, True)
            self.lock[k].pop(p)

            # SIGNAL waiting reads, each try to read the val for k
            i = p
            while i < len(self.lock[k]) and not self.lock[k][i].isWrite:
                rtx = self.lock[k][i].transaction
                msg += self.__serversRead__(rtx, k)
                i += 1

            i = 0
            while i < len(self.lock[k]) and not self.lock[k][i].isWrite:
                rtx = self.lock[k][i].transaction
                if not rtx.reads[k]:
                    msg += self.__serversRead__(rtx, k)
                i += 1

            # SIGNAL read-only transactions
            for rotx in self.aliveTx.values():
                if rotx.RO and k in rotx.reads and not rotx.reads[k]:
                    msg += self.__serversRead__(rotx, k)

        for k in tx.reads:
            i = 0
            while i < len(self.lock[k]):
                if self.lock[k][i].transaction == tx:
                    self.lock[k].pop(i)
                else:
                    i += 1

        del self.aliveTx[tx.id]

        return msg