from collections import defaultdict

class Transaction:
    def __init__(self, begin, id, readOnly=False):
        self.id = id
        self.begin = begin
        self.writes = defaultdict(list) # key:value item:[list of vals to write]
        self.reads = {} # key:value item:value get
        self.RO = readOnly # read only
